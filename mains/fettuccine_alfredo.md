# Fettuccine Alfredo ![](https://badgen.net/badge/tried/no/red)
A fettuccine alfredo recipe with veggies.
Goes well with Gardein Chick'n Scallopini.

## Ingredients (for 4 people)
* ~350g ounces Fettuccine pasta
* 1 cup raw cashews soaked overnight or at least 6 hours.
* 1 teaspoon plant-based butter
* 1 teaspoon olive oil
* &frac14; cup hot water (use pasta water)
* 1 &frac12; cups unsweetened almond milk
* 1 tablespoon lemon juice
* &frac12; teaspoon sea salt
* 1 tablespoon Bragg Liquid Amino's or soy sauce
* 3 tablespoons nutritional yeast
* &frac12; cup red sweet bell pepper (diced small)
* &frac14; teaspoon dried rosemary
* &frac12; teaspoon asafoteida (hing) powder (alternatively, some fresh onions, finely chopped)
* &frac14; teaspoon black pepper
* 1 cup broccoli tops (no stem)
* &frac12; cup cauliflower chopped small
* &frac12; yellow zucchini cubed small
* 2 cups fresh chopped spinach (no stems)

## Instructions

* Cook pasta according to directions
* While the pasta is cooking:
  in a pan over medium heat put the butter and olive oil.
  + Add the red peppers and asafotieda and cook 'til tender 4-5 minutes.
  + Remove from the pan and add it to a food processor.
  + Add the cashews, pasta water, almond milk, nutritional yeast, salt,
    lemon juice, Bragg's (or soy sauce), rosemary and black pepper.
  + Blend it all together until completely creamy and set aside.
* Next steam your veggies (except spinach). Don't over steam. Set aside with a lid to keep them warm.
* When the pasta is finished, drain well and put in a large bowl.
* Pour the Alfredo sauce and mix thoroughly
* Next add your steamed veggies and fold in your spinach. Mix until spinach is wilted.
