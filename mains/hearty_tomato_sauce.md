# Hearty Tomato Sauce ![](https://badgen.net/badge/tried/yes/green)
A hearty tomato sauce which goes great with any pasta.
It's prepared in 20 minutes, making it an ideal weekday dinner.

## Ingredients (for 2 people)
* ~200g of pasta you like
* 3 tomatoes
* 1 tablespoon of tomato paste
* 1 medium red onion
* 1 clove of garlic
* 1 tablespoon of olive oil
* 1 teaspoon of sugar
* 0.5 dl water
* salt, pepper, and paprika to taste

## Instructions
* Start pasta water cooking
* Chop the tomatoes into cubes (5-10mm edges)
* Heat the oil on high in a non-stick frying pan
* Finely chop the onions and garlic
* Add the onions, garlic, tomato paste, some salt and paprika to the pan
* Stir until slightly browning
* Add tomatoes, sugar, and water
* Stir to mix everything
* Cover for 4-5 minutes, then remove cover and turn heat to low
* Simmer until the sauce reaches the desired consistency
* Add salt and peper to taste.
