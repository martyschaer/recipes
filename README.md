# Recipes
This repo contains some recipes. Each will say whether or not I've already cooked it myself.

With badges from [badgen.net](https://badgen.net/).

# Index
## Starters
* [Creamy Almond Dressing](https://gitlab.com/martyschaer/recipes/blob/master/starters/creamy_almond_dressing.md )

## Mains
* [Fettuccine Alfredo](https://gitlab.com/martyschaer/recipes/blob/master/mains/fettuccine_alfredo.md )
* [Hearty Tomato Sauce](https://gitlab.com/martyschaer/recipes/blob/master/mains/hearty_tomato_sauce.md )

## Desserts
* [Tofu Tiramisu](https://gitlab.com/martyschaer/recipes/blob/master/desserts/tofu_tiramisu.md )
* [Vanilla Cream](https://gitlab.com/martyschaer/recipes/blob/master/desserts/vanilla_cream.md )

## Breakfast
* [Pancakes](https://gitlab.com/martyschaer/recipes/blob/master/breakfast/pancakes.md )
