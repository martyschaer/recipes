# Creamy Almond Dressing ![](https://badgen.net/badge/tried/no/red)
A dressing to use with your favourite salad.

## Ingredients
* 1 cup raw almonds
* 1 cup nutritional yeast
* &frac14; cup olive oil
* 2 cups water
* &frac34; teaspoon sea salt
* 4 tablespoons Bragg Liquid Aminos or soy sauce

## Instructions
* Blend in food processor
