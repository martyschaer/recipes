# Pancakes ![](https://badgen.net/badge/tried/yes/green)
The family recipe for pancakes, altered to be vegan.

## Ingredients (for 4 people)
* Dry ingredients
  + 300g flour
  + 1 Pack (16g) baking powder
  + 2 tablespoons sugar
  + &frac34; tablespoons salt 
* Wet ingredients
  + 2 tablespoons unsweetened peanut butter
  + 3.7 dl of unsweetended almond milk
  + 2 medium bananas, very ripe

## Instructions
* Mix dry ingredients in a bowl
* Squish banana, together with oil and some of the milk
* Add banana-butter-milk mixture to dry ingredients
* Add rest of milk to dry ingredients
* Mix well
* Let rest for 10-15 minutes
* Bake on low-medium heat in a non-stick pan.
