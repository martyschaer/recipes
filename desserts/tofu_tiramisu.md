# Tofu Tiramisu ![](https://badgen.net/badge/tried/yes/green)
This is a delicious vegan alternative to tiramisu.

## Ingredients (1 portion)
* 1 package silken tofu
* 1 package vanilla sugar
* 6 tablespoons sugar
* 2 tablespoons water (or soy milk)
* 1 lemon worths of lemon zest
* 3 vegan ladyfingers or other absorbant biscuit
* 2 double espressi
* 1 tablespoon amaretto
* 1 tablespoon of orange liqueur
* Decorative cocao powder

## Instructions
* Add tofu, sugar, water, and lemon to a food processor
* Blend until smooth
* Build a tiramisu in a small bowl
  * Build a layer of biscuits
  * Soak with the coffee
  * Add the cream from the food processor
* Store in fridge for 4h
* Before serving, springle with some cocao powder
