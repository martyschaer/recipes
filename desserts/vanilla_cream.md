# Vanilla Cream ![](https://badgen.net/badge/tried/yes/green)
A vegan alternative to vanilla cream.

## Ingredients (4 portions)
* 300ml soy milk with vanilla flavour 
* 70g cashews
* 6g potato starch
* 5 tablespoons of yacoun syrup (alternatively use maple syrup)
* 1 teaspoon of vanilla extract
* &frac12; teaspoon of salt

## Instructions
* Add everything to a food processor
* Bring to a boil, while stiring
* Cook for 1 minute
* Let cool for 4 hours
